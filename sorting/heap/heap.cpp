#include "heap.h"

Heap::Heap(int* a, int n) {
  data = a;
  size = n;
  for (int i = size / 2; i >= 1; --i) {
    maxHeapify(i);
  }
}

void Heap::maxHeapify(int i) {
  int l = LEFT(i);
  int r = RIGHT(i);

  int largest;

  if (l <= size && data[l] > data[i]) {
    largest = l;
  }
  else {
    largest = i;
  }

  if (r <= size && data[r] > data[largest]) {
    largest = r;
  }

  if (largest != i) {
    int tmp = data[largest];
    data[largest] = data[i];
    data[i] = tmp;
    maxHeapify(largest);
  }
}

int Heap::LEFT(int i) {
  return 2 * i;
}

int Heap::RIGHT(int i) {
  return 2 * i + 1;
}

int Heap::PARENT(int i) {
  return i / 2;
}


#ifndef HEAP_H
#define HEAP_H

class Heap {
public:
  Heap(int*, int);
  void maxHeapify(int);
  int size;
private:
  int LEFT(int);
  int RIGHT(int);
  int PARENT(int);
  int* data;
};

#endif

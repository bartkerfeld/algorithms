#include <iostream>
#include "heap.h"

using namespace std;

void heapsort(int a[], int size) {
  --size; // Ignoring first index so size is 1 less
  Heap h(a, size);
  for (int i = size; i >= 2; --i) {
    int tmp = a[1];
    a[1] = a[i];
    a[i] = tmp;
    h.size--;
    h.maxHeapify(1);
  }
}

void printArray(int a[], int size) {
  for (int i = 1; i < size; ++i) {
    if (i % 20 == 0) {
      cout << endl;
    }
    cout << a[i] << " ";
  }
  cout << endl << endl;
}

int main() {
  const int ARRAY_SIZE = 100;
  const int MIN_RAND = 100;
  const int MAX_RAND = 1000 - MIN_RAND;

  int* a = new int[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; ++i) {
    a[i] = MIN_RAND + (rand() % MAX_RAND);
  }

  printArray(a, ARRAY_SIZE);
  heapsort(a, ARRAY_SIZE);
  printArray(a, ARRAY_SIZE);

  delete [] a;
  return 0;
}


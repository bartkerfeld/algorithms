#include <iostream>

using namespace std;

void merge(int a[], int p, int q, int r) {
  int n1 = q - p + 1;
  int n2 = r - q;

  int* left = new int[n1];
  int* right = new int[n2];
  for (int i = 0; i < n1; ++i) {
    left[i] = a[p + i];
  }
  for (int j = 0; j < n2; ++j) {
    right[j] = a[q + j + 1];
  }

  int i = 0, j = 0;

  for (int x = p; x <= r; ++x) {
    if (i >= n1) {
      a[x] = right[j];
      ++j;
    }
    else if (j >= n2) {
      a[x] = left[i];
      ++i;
    }
    else if (left[i] < right[j]) {
      a[x] = left[i];
      ++i;
    }
    else {
      a[x] = right[j];
      ++j;
    }
  }

  delete [] left;
  delete [] right;
}

void mergesort(int a[], int p, int r) {
  if (p < r) {
    int q = (p + r) / 2;
    mergesort(a, p, q);
    mergesort(a, q + 1, r);
    merge(a, p, q, r);
  }
}

void printArray(int a[], int n) {
  for (int i = 0; i < n; ++i) {
    if (i % 20 == 0) {
      cout << endl;
    }
    cout << a[i] << " ";
  }
  cout << endl << endl;
}

int main() {
  const int ARRAY_SIZE = 100;
  const int MIN_RAND = 10;
  const int MAX_RAND = 1000 - MIN_RAND;

  int* a = new int[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; ++i) {
    a[i] = MIN_RAND + (rand() % MAX_RAND);
  }

  printArray(a, 100);

  mergesort(a, 0, ARRAY_SIZE - 1);

  printArray(a, 100);

  delete [] a;
  return 0;
}

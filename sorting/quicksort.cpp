#include <iostream>

using namespace std;

// Function Declartions
void printArray(int a[], int n);
int partition(int a[], int p, int r);
int randomized_partition(int a[], int p, int r);

void quicksort(int a[], int p, int r) {
  if (p < r) {
    int q = randomized_partition(a, p, r);
    quicksort(a, p, q - 1);
    quicksort(a, q + 1, r);
  }
}

int quickselect(int a[], int p, int r, int i) {
  if (p == r) {
    return a[p];
  }

  int q = randomized_partition(a, p, r);
  int k = q - p + 1;
  if ( i == k ) {
    return a[q];
  }
  else if ( i < k ) {
    return quickselect(a, p, q - 1, i);
  }
  else {
    return quickselect(a, q + 1, r, i - k);
  }
}

int partition(int a[], int p, int r) {
  int x = a[r];
  int i = p - 1;
  for (int j = p; j < r; ++j) {
    if (a[j] <= x) {
      i = i + 1;
      int tmp = a[i];
      a[i] = a[j];
      a[j] = tmp;
    }
  }
  int tmp = a[i + 1];
  a[i + 1] = a[r];
  a[r] = tmp;
  return (i + 1);
}

int randomized_partition(int a[], int p, int r) {
  int i = ( rand() % (r - p)) + p;
  int tmp = a[r];
  a[r] = a[i];
  a[i] = tmp;
  partition(a, p, r);
}


int main() {
  int* a = new int[5] {0, 1, 4, 3, 2};
  int* b = new int[5] {0, 1, 4, 3, 2};

  printArray(a, 5);
  quicksort(a, 1, 4);
  printArray(a, 5);

  int ithSmallest = quickselect(a, 1, 4, 2);
  cout << "The 2nd smallest is " << ithSmallest << endl;

  return 0;
}

void printArray(int a[], int n) {
  for (int i = 1; i < n; ++i) {
    cout << a[i] << " ";
  }
  cout << endl;
}
